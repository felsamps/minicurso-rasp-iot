import paho.mqtt.client as mqtt
from time import sleep
import sys
import random

#definitions
Broker = "m14.cloudmqtt.com"
Port = 12891
KeepAliveBroker = 60

username = "vqlvmcfj"
password = "Vzw6NIX4voxY"

sensor1Topic = "sensor1_01"
sensor2Topic = "sensor2_02"

#Callback - broker connection
def on_connect(client, userdata, flags, rc):
    print "[STATUS] Conectado ao Broker. Resultado da conexao: " + str(rc)


def on_message(client, userdata, msg):
    print "[MSG RECEBIDA] Topico:", msg.topic, "/ Mensagem:", message

if __name__ == "__main__":
    print "[STATUS] Inicializando MQTT..."

    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message
    client.username_pw_set(username, password)

    client.connect(Broker, Port, KeepAliveBroker)
    client.loop_start()

    while True:
        sensor1Value = 20.0 + random.randint(0,1000)/100.0
        sensor2Value = 100.0 + random.randint(0,1000)/50.0

        #print sensor1Value, sensor2Value

        client.publish(sensor1Topic, str(sensor1Value))
        client.publish(sensor2Topic, str(sensor2Value))
        sleep(1)

