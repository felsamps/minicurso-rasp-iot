import paho.mqtt.client as mqtt
from gpiozero import LED, PWMLED
from signal import pause
import sys

#definitions
Broker = "m14.cloudmqtt.com"
Port = 12891
KeepAliveBroker = 60

username = "vqlvmcfj"
password = "Vzw6NIX4voxY"

ledTopic = "led01"
ledPWMTopic = "ledPWM01"

#Callback - broker connection
def on_connect(client, userdata, flags, rc):
    print "[STATUS] Conectado ao Broker. Resultado da conexao: " + str(rc)

    client.subscribe(ledTopic)
    client.subscribe(ledPWMTopic)

def on_message(client, userdata, msg):
    message = str(msg.payload)
    print "[MSG RECEBIDA] Topico:", msg.topic, "/ Mensagem:", message
    if msg.topic == ledTopic:
        if message == "ON":
            led.on()
        else:
            led.off()

    if msg.topic == ledPWMTopic:

        valor = float(float(message)/100.0)
        print(valor)
        ledPWM.value = valor

if __name__ == "__main__":
    led = LED(17)
    ledPWM = PWMLED(27)

    led.off()
    ledPWM.value = 0.5

    print "[STATUS] Inicializando MQTT..."

    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message
    client.username_pw_set(username, password)

    client.connect(Broker, Port, KeepAliveBroker)
    client.loop_start()

    pause()
