from flask import Flask
app = Flask(__name__)

@app.route("/")
def hello():
    return "Bem vindo ao minicurso de Programação Raspbery Pi para Internet das Coisas!"

#TODO change ip address
app.run(debug = True, port = 8000, host =  '192.168.0.102')
