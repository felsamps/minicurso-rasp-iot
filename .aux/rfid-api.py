from flask import Flask, json
app = Flask(__name__)

@app.route("/verifica/<id>")
def verifica(id):
    response = app.response_class(
        response = json.dumps(jsonObj[id]),
        status = 200,
        mimetype = 'application/json'
    )
    return response

jsonFile = open('verifica-acesso.json','r')
jsonContent = ''.join(jsonFile.readlines())
jsonObj = json.loads(jsonContent)

app.run(debug = True, port = 8000, host =  '192.168.0.102')
