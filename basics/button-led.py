from gpiozero import Button, LED
from signal import pause

def changeStatusLED():
    global stateLED
    stateLED = not stateLED
    print(stateLED)


led = LED(17)
button = Button(2)

stateLED = True

button.when_pressed = changeStatusLED

while True:
    if stateLED:
        led.on()
    else:
        led.off()
