from gpiozero import PWMLED
from time import sleep

led = PWMLED(17)
lumin = 0.5
step = 0.05

while True:
    if lumin > 0.9 or lumin < 0.1:
        step = - step

    lumin += step
    led.value = lumin

    sleep(0.1)

