from gpiozero import LED
from time import sleep

timeInterval = 1    #1 second
led = LED(17)

while True:
    led.on()
    sleep(timeInterval)
    led.off()
    sleep(timeInterval)
