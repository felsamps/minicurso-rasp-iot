from gpiozero import Servo
from time import sleep

def convertAngle(angle):
    return angle * (2.0 / 180) - 1

pinServo = 4
servo = Servo(pinServo)

while True:
    for ang in range(0, 180, 5):
        servo.value = convertAngle(ang)
        sleep(0.05)


