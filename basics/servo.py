from gpiozero import Servo
from time import sleep

pinServo = 4

servo = Servo(pinServo)

while True:
    servo.min()
    print (servo.value)
    sleep(1)
    servo.mid()
    print (servo.value)
    sleep(1)
    servo.max()
    print (servo.value)
    sleep(1)

