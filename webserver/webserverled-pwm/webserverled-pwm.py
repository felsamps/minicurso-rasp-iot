from  flask import Flask, render_template, request
from gpiozero import PWMLED

app = Flask(__name__)

@app.route('/')

def index():

    ledString = str(int (led_brightness * 100))

    templateData = {
            'title' : 'LED manager',
            'led_status' : ledString,
            }

    return render_template('led.html', **templateData)

@app.route('/led')

def changeBrightness():

    led_brightness = float(request.args.get('ledbrightness')) / 100.0

    print led_brightness

    led.value = led_brightness

    ledString = str(int (led_brightness * 100))

    templateData = {
            'title' : 'LED manager',
            'led_status' : ledString,
            }

    return render_template('led.html', **templateData)


if __name__ == '__main__':

    led_brightness = 0.0

    led = PWMLED(17)
    led.value = led_brightness


    app.run(debug = True, port = 8000, host =  '127.0.0.1')
