from  flask import Flask, render_template
from gpiozero import LED, Button

app = Flask(__name__)

@app.route('/')

def index():

    ledStatusString = "On" if led_status else "Off"

    if led_status:
        led.on()
    else:
        led.off()

    templateData = {
            'title' : 'LED manager',
            'led_status' : ledStatusString,
            }

    return render_template('led.html', **templateData)

@app.route('/led/<action>')

def action(action):
    if action == 'on':
        led_status = True
        led.on()
    if action == 'off':
        led_status = False
        led.off()

    ledStatusString = "On" if led_status else "Off"

    templateData = {
            'title' : 'LED manager',
            'led_status' : ledStatusString,
            }

    return render_template('led.html', **templateData)


if __name__ == '__main__':

    led_name = 'led'
    led_status = True
    led = LED(17)
    led.on()


    app.run(debug = True, port = 8000, host =  '127.0.0.1')
