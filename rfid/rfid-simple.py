import signal
import time
import sys

from pirc522 import RFID

rdr = RFID()

print("Starting")

while True:
    rdr.wait_for_tag()
    (error, data) = rdr.request()
    if not error:
        print("\nDetected: " + format(data, "02x"))
        (error, uid) = rdr.anticoll()
        if not error:
            uidTag = hex(uid[0]).lstrip('0x') + hex(uid[1]).lstrip('0x') + hex(uid[2]).lstrip('0x') + hex(uid[3]).lstrip('0x')
            print("Card read UID: " + uidTag)
            time.sleep(1)
