import paho.mqtt.client as mqtt
import sys

#definitions
Broker = "m14.cloudmqtt.com"
Port = 12891
KeepAliveBroker = 60

username = "vqlvmcfj"
password = "Vzw6NIX4voxY"

rfidTopic = "rfid01"

#Callback - broker connection
def on_connect(client, userdata, flags, rc):
    print "[STATUS] Conectado ao Broker. Resultado da conexao: " + str(rc)

def on_message(client, userdata, msg):
    message = str(msg.payload)
    print "[MSG RECEBIDA] Topico:", msg.topic, "/ Mensagem:", message

if __name__ == "__main__":
    rdr = RFID()

    print "[STATUS] Inicializando MQTT..."

    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message
    client.username_pw_set(username, password)

    client.connect(Broker, Port, KeepAliveBroker)
    client.loop_start()

    while True:
        rdr.wait_for_tag()
        (error, data) = rdr.request()
        if not error:
            print("\nDetected: " + format(data, "02x"))
            (error, uid) = rdr.anticoll()
            if not error:
                uidTag = hex(uid[0]).lstrip('0x') + hex(uid[1]).lstrip('0x') + hex(uid[2]).lstrip('0x') + hex(uid[3]).lstrip('0x')
                publish(rfidTopic,uid)
                time.sleep(1)

